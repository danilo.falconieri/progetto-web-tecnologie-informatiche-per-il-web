package it.polimi.controllers;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;

import it.polimi.DAO.InvitatoDAO;
import it.polimi.DAO.RiunioneDAO;
import it.polimi.DAO.UtenteDAO;
import it.polimi.beans.RiunioneBean;

/**
 * Servlet implementation class GetRiunioni
 */
@WebServlet("/GetRiunioni")
public class GetRiunioni extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public GetRiunioni() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		// controllo sessione attiva
		HttpSession sessione = request.getSession(false);

		if (sessione == null) {
			String URL = "Index.html";
			response.setStatus(777);
			response.setHeader("Location", URL);
			return;
		}
		// controllo utente loggato correttamente; STATO 777 SOLO TEST
		String user = (String) sessione.getAttribute("user");
		if (user == null) {
			String URL = "Index.html";
			response.setStatus(777);
			response.setHeader("Location", URL);
			return;
		}
		
		Connection connection = openConnection();
		
		if (connection == null) {
		    response.sendError(500, "Errore di connessione al database. Ritorna alla pagina iniziale.");
		    return;
		}

		// richiedo a RiunioneDAO le riunioni organizzate dallo user della sessione in
		// questione
		List<RiunioneBean> riunioniOrganizzate = new ArrayList<RiunioneBean>();
		RiunioneDAO riunioneDAO = new RiunioneDAO(connection);
		try {
			riunioniOrganizzate = riunioneDAO.getRiunioniValideOrganizzate(user);
		} catch (SQLException e) {
			response.sendError(500, "Errore di connessione al database.");
			closeConnection(connection);
			return;
		}

		// richiedo ad InvitatoDAO le riunioni a cui � stato invitato lo user della
		// sessione in questione
		List<RiunioneBean> riunioniInvitato = new ArrayList<RiunioneBean>();
		InvitatoDAO invitatoDAO = new InvitatoDAO(connection);
		UtenteDAO utenteDAO = new UtenteDAO(connection);
		try {
			riunioniInvitato = invitatoDAO.getInvitiValidi(utenteDAO.getIdUtente(user));
		} catch (SQLException e) {
			response.sendError(500, "Errore di connessione al database.");
			closeConnection(connection);
			return;
		}

		// raggruppo le due liste in un unico array
		ArrayList<List<RiunioneBean>> listeRiunioni = new ArrayList<List<RiunioneBean>>();
		listeRiunioni.add(riunioniOrganizzate);
		listeRiunioni.add(riunioniInvitato);

		// creo l'oggetto json per inviare le due liste
		String listeRiunioniJson = new Gson().toJson(listeRiunioni);

		response.setStatus(200);
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write(listeRiunioniJson);

		closeConnection(connection);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

	private Connection openConnection() {
		Connection connection = null;
		boolean errDB = true;
		int tentativi = 0;
		while (errDB) {
			if (connection == null) {
				try {
					ServletContext context = getServletContext();
					String driverDB = context.getInitParameter("dbDriver");
					String urlDB = context.getInitParameter("dbUrl");
					String userDB = context.getInitParameter("dbUser");
					String passwordDB = context.getInitParameter("dbPassword");
					Class.forName(driverDB);
					connection = DriverManager.getConnection(urlDB, userDB, passwordDB);
				} catch (ClassNotFoundException | SQLException e) {
					connection = null;
					errDB = true;
					if (++tentativi == 10) {
						return null;
					}
				}
			}
			errDB = false;
		}
		return connection;
	}

	private void closeConnection(Connection connection) {
		try {
			if (connection != null)
				connection.close();
		} catch (SQLException e) {
			System.err.println("Error closing connection to database: " + e.toString());
		}
	}
}
