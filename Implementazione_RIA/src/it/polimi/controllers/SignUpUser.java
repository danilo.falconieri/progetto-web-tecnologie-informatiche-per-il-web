package it.polimi.controllers;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import it.polimi.DAO.UtenteDAO;


@WebServlet("/SignUpUser")
public class SignUpUser extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
    public SignUpUser() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.sendRedirect(request.getContextPath() + "/Index.html");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Connection connection = openConnection();
		
		String mail = request.getParameter("mail");
		String nome = request.getParameter("nome");
		String cognome = request.getParameter("cognome");
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		String ripetiPassword = request.getParameter("ripetiPassword");
		
		String regexMail = "^[\\w-_\\.]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
		String regexNomeCognome = "^[a-zA-Z]{2,}$";
		String regexUsername = "^[a-zA-Z][a-zA-Z0-9.]{3,}$";
		String regexPassword = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).{8,}$";
		if(!mail.matches(regexMail) || !nome.matches(regexNomeCognome) || !cognome.matches(regexNomeCognome) || !username.matches(regexUsername) || !password.equals(ripetiPassword) || !password.matches(regexPassword) || connection == null) {
			response.setStatus(472);
		} else {
			UtenteDAO utenteDAO = new UtenteDAO(connection);
			try {
				boolean successo = utenteDAO.insertUser(mail, username, nome, cognome, password);
				if(successo == false) {
					response.setStatus(472);
				} else {
					response.setStatus(200);
				}
			} catch (SQLException e) {
				response.setStatus(472);
				closeConnection(connection);
				return;
			}
		}

		closeConnection(connection);
	}
		
	private Connection openConnection() {
		Connection connection = null;
		boolean errDB = true;
		int tentativi = 0;
		while (errDB) {
			if (connection == null) {
				try {
					ServletContext context = getServletContext();
					String driverDB = context.getInitParameter("dbDriver");
					String urlDB = context.getInitParameter("dbUrl");
					String userDB = context.getInitParameter("dbUser");
					String passwordDB = context.getInitParameter("dbPassword");
					Class.forName(driverDB);
					connection = DriverManager.getConnection(urlDB, userDB, passwordDB);
				} catch (ClassNotFoundException | SQLException e) {
					connection = null;
					errDB = true;
					if(++tentativi == 10) {
						return null;
					}
				}
			}
			errDB = false;
		}
		return connection;
	}

	private void closeConnection(Connection connection) {
		try {
			if (connection != null)
				connection.close();
		} catch (SQLException e) {
			System.err.println("Error closing connection to database: " + e.toString());
		}
	}
}
