package it.polimi.controllers;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import it.polimi.DAO.UtenteDAO;

/**
 * Servlet implementation class CheckLogin
 */
@WebServlet("/CheckLogin")
public class CheckLogin extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public CheckLogin() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// controllo sessione attiva
		HttpSession sessione = request.getSession(false);
		if (sessione != null) {
			sessione.invalidate();
		}

		// se i parametri ricevuti sono nulli, comunico con un errore 470 che
		// l'autenticazione
		// � fallita e mi fermo
		
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		if (username == null || username.isEmpty() || password == null 
				 || password.isEmpty()) {
			response.setStatus(470);
			response.getWriter().write("Username o Password vuoti");
			return;
		}

		Connection connessione = openConnection();
		
		if (connessione == null) {
		    response.sendError(500, "Errore di connessione al database. Ritorna alla pagina iniziale.");
		    return;
		}

		// altrimenti, prelevo i parametri ricevuti e verifico che siano corretti
		// attraverso UtenteDAO
		
		UtenteDAO utenteDAO = new UtenteDAO(connessione);
		// se i dati non sono corretti, comunico il codice 471 cio� che l'autenticazione
		// �
		// fallita e mi fermo
		try {
			if (!utenteDAO.controlloUserPsw(username, password)) {
				response.setStatus(471);
				response.getWriter().write("Username o Password sbagliati");
				closeConnection(connessione);
				return;
			}
		} catch (SQLException e) {
			response.sendError(500, "Errore di connessione al database. Ritorna alla pagina iniziale.");
			closeConnection(connessione);
			return;
		}
		// se i dati sono corretti, creo una sessione valida per l'utente e rimando lo
		// status 200
		sessione = request.getSession(true);
		sessione.setMaxInactiveInterval(1800);
		sessione.setAttribute("user", username);
		response.setStatus(200);
		closeConnection(connessione);

	}

	private Connection openConnection() {
		Connection connection = null;
		boolean errDB = true;
		int tentativi = 0;
		while (errDB) {
			if (connection == null) {
				try {
					ServletContext context = getServletContext();
					String driverDB = context.getInitParameter("dbDriver");
					String urlDB = context.getInitParameter("dbUrl");
					String userDB = context.getInitParameter("dbUser");
					String passwordDB = context.getInitParameter("dbPassword");
					Class.forName(driverDB);
					connection = DriverManager.getConnection(urlDB, userDB, passwordDB);
				} catch (ClassNotFoundException | SQLException e) {
					connection = null;
					errDB = true;
					if (++tentativi == 10) {
						return null;
					}
				}
			}
			errDB = false;
		}
		return connection;
	}

	private void closeConnection(Connection connection) {
		try {
			if (connection != null)
				connection.close();
		} catch (SQLException e) {
			System.err.println("Error closing connection to database: " + e.toString());
		}
	}

}
