package it.polimi.controllers;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;

import it.polimi.DAO.UtenteDAO;
import it.polimi.beans.UtenteBean;

/**
 * Servlet implementation class GetDatiAnagrafica
 */
@WebServlet("/GetDatiAnagrafica")
public class GetDatiAnagrafica extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public GetDatiAnagrafica() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//controllo sessione attiva
		HttpSession sessione = request.getSession(false);
		
		 if (sessione == null) {
			String URL = "Index.html";
			response.setStatus(777);
			response.setHeader("Location", URL);	
			return;
		}

		String user = (String) sessione.getAttribute("user");
		if (user == null) {
			String URL = "Index.html";
			response.setStatus(777);
			response.setHeader("Location", URL);
			return;
		} 

		Connection connection = openConnection();
		
		if (connection == null) {
		    response.sendError(500, "Errore di connessione al database. Ritorna alla pagina iniziale.");
		    return;
		}

		//Ottengo i dati degli utenti dal db
		UtenteDAO utenteDAO = new UtenteDAO(connection);
		List<UtenteBean> listaUtenti = null;
		try {
			listaUtenti = utenteDAO.getAllDataWithoutPsw(user);
		} catch (SQLException e) {
			response.sendError(500, "Errore di connessione al database. Ritorna alla pagina iniziale.");
			closeConnection(connection);
			return;
		}

		//converto in json la listaUtenti
		String json = new Gson().toJson(listaUtenti);
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write(json);
		
		closeConnection(connection);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

	//apre connessione verso il db, prelevando i parametri contenuti in web.xml
	private Connection openConnection() {
		Connection connection = null;
		boolean errDB = true;
		int tentativi = 0;
		while (errDB) {
			if (connection == null) {
				try {
					ServletContext context = getServletContext();
					String driverDB = context.getInitParameter("dbDriver");
					String urlDB = context.getInitParameter("dbUrl");
					String userDB = context.getInitParameter("dbUser");
					String passwordDB = context.getInitParameter("dbPassword");
					Class.forName(driverDB);
					connection = DriverManager.getConnection(urlDB, userDB, passwordDB);
				} catch (ClassNotFoundException | SQLException e) {
					connection = null;
					errDB = true;
					if (++tentativi == 10) {
						return null;
					}
				}
			}
			errDB = false;
		}
		return connection;
	}

	//apre connessione verso il db
	private void closeConnection(Connection connection) {
		try {
			if (connection != null)
				connection.close();
		} catch (SQLException e) {
			System.err.println("Error closing connection to database: " + e.toString());
		}
	}
}
