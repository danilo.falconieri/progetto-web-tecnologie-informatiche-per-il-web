package it.polimi.controllers;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import it.polimi.DAO.UtenteDAO;


@WebServlet("/CheckUser")
public class CheckUser extends HttpServlet {
	private static final long serialVersionUID = 1L;
   
    public CheckUser() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Connection connection = openConnection();
		
		if(connection == null) {
			response.setStatus(472);
			return;
		}
		
		UtenteDAO utenteDAO = new UtenteDAO(connection);
		try {
			boolean disponibile = utenteDAO.checkUser(request.getParameter("username"));
			if(disponibile == false) {
				response.setStatus(471);
			} else {
				response.setStatus(200);
			}
		} catch (SQLException e) {
			response.setStatus(472);
			closeConnection(connection);
			return;
		}
		
		closeConnection(connection);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

	private Connection openConnection() {
		Connection connection = null;
		boolean errDB = true;
		int tentativi = 0;
		while (errDB) {
			if (connection == null) {
				try {
					ServletContext context = getServletContext();
					String driverDB = context.getInitParameter("dbDriver");
					String urlDB = context.getInitParameter("dbUrl");
					String userDB = context.getInitParameter("dbUser");
					String passwordDB = context.getInitParameter("dbPassword");
					Class.forName(driverDB);
					connection = DriverManager.getConnection(urlDB, userDB, passwordDB);
				} catch (ClassNotFoundException | SQLException e) {
					connection = null;
					errDB = true;
					if(++tentativi == 10) {
						return null;
					}
				}
			}
			errDB = false;
		}
		return connection;
	}

	private void closeConnection(Connection connection) {
		try {
			if (connection != null)
				connection.close();
		} catch (SQLException e) {
			System.err.println("Error closing connection to database: " + e.toString());
		}
	}
}
