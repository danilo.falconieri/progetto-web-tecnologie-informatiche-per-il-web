package it.polimi.controllers;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import it.polimi.DAO.InvitatoDAO;
import it.polimi.DAO.RiunioneDAO;
import it.polimi.DAO.UtenteDAO;

@WebServlet("/AggiornaDB")
public class AggiornaDB extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public AggiornaDB() {
	super();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, IOException {

	doGet(request, response);

    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, IOException {
	// Controllo che la sessione sia ancora valida, in caso contrario ritorno alla
	// pagina iniziale dell'applicazione
	HttpSession sessione = request.getSession(false);

	if (sessione == null) {
	    String URL = "Index.html";
	    response.setStatus(777);
	    response.setHeader("Location", URL);
	    return;
	}
	// controllo utente loggato correttamente;
	String user = (String) sessione.getAttribute("user");
	if (user == null) {
	    String URL = "Index.html";
	    response.setStatus(777);
	    response.setHeader("Location", URL);
	    return;
	}

	// Prendo i dati relativi alla riunione da creare, alla lista utenti selezionati
	// e al numero di tentativi e li controllo

	// conrollo tentativi
	Integer nTentativi = 0;
	try {
	    nTentativi = Integer.parseInt(request.getParameter("nTentativi"));
	} catch (NumberFormatException e) {
	    response.setStatus(472);
	    return;
	}
	if (nTentativi < 1 || nTentativi > 3 || nTentativi == null) {
	    response.setStatus(472);
	    return;
	}

	// controllo titolo
	String titolo = request.getParameter("titolo");
	if (titolo == null || titolo == "") {
	    response.setStatus(470);
	    return;
	}

	// controllo data
	String dataeora = request.getParameter("dataeora");
	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
	dateFormat.setLenient(false);
	Date dataInserita = null;
	try {
	    dataInserita = dateFormat.parse(dataeora.trim());
	} catch (Exception e) {
	    response.setStatus(470);
	    return;
	}

	// se non ho avuto errori nel formato (esiste la data)
	if (dataInserita.before(new Date())) {
	    response.setStatus(470);
	    return;
	}

	// controllo durata
	String durata = request.getParameter("durata");

	int hhDurata = Integer.valueOf(durata.split(":")[0]);
	int mmDurata = Integer.valueOf(durata.split(":")[1]);
	boolean errRangeDurata = false;
	if (hhDurata == 0) {
	    if (mmDurata < 15 || mmDurata > 59)
		errRangeDurata = true;
	} else if (hhDurata == 1 || hhDurata == 2 || hhDurata == 3) {
	    if (mmDurata < 00 || mmDurata > 59)
		errRangeDurata = true;
	} else if (hhDurata == 4) {
	    if (mmDurata > 00)
		errRangeDurata = true;
	} else {
	    errRangeDurata = true;
	}
	if (errRangeDurata) {
	    response.setStatus(470);
	    return;
	}

	// controllo numPartecipanti
	int numPartecipanti = 0;
	try {
	    numPartecipanti = Integer.parseInt(request.getParameter("numPartecipanti"));
	} catch (NumberFormatException e) {
	    response.setStatus(470);
	    return;
	}
	if (numPartecipanti < 1) {
	    response.setStatus(470);
	    return;
	}

	// controllo lista utenti selezionati
	ArrayList<Integer> utentiSelezionati = new ArrayList<Integer>();

	Connection connection = openConnection();
	if (connection == null) {
	    response.sendError(500, "Errore di connessione al database. Ritorna alla pagina iniziale.");
	    return;
	}
	UtenteDAO utenteDAO = new UtenteDAO(connection);
	int idUtente = -1;
	try {
	    idUtente = utenteDAO.getIdUtente(user);
	} catch (SQLException e1) {
	    response.sendError(500, "Errore di connessione al database. Ritorna alla pagina iniziale.");
	    closeConnection(connection);
	    return;
	}

	// prelevo la lista di selezionati e controllo
	for (String sel : request.getParameterValues("selezionati")) {
	    String usernameSel = sel.split(";")[1];

	    int idSel = 0;
	    try {
		idSel = Integer.parseInt(sel.split(";")[0]);
	    } catch (NumberFormatException e) {
		response.setStatus(471);
		return;
	    }

	    //verifico che non mi sia autoinvitato
	    if (idSel == idUtente) {
		response.setStatus(471);
		return;
	    }

	    //controllo che id e username coincidano
	    try {
		if (!utenteDAO.checkIdUtenteUsername(idSel, usernameSel)) {
		    response.setStatus(471);
		    return;
		}
	    } catch (SQLException e) {
		response.sendError(500, "Errore di connessione al database. Ritorna alla pagina iniziale.");
		return;
	    }

	    utentiSelezionati.add(idSel);
	}

	//verifico che non vi sia lo stesso utente selezionato due volte
	for (int i = 0; i < utentiSelezionati.size(); i++) {
	    for (int j = 0; j < utentiSelezionati.size(); j++) {
		if (utentiSelezionati.get(i) == utentiSelezionati.get(j) && i != j) {
		    response.setStatus(471);
		    return;
		}
	    }
	}

	// Controllo riunione organizzata non sia duplicata
	RiunioneDAO riunioneDAO = new RiunioneDAO(connection);
	boolean duplicata;
	try {
	    duplicata = riunioneDAO.verificaRiunioneDuplicata(dataeora, user);
	} catch (SQLException e1) {
	    response.sendError(500, "Errore di connessione al database. Ritorna alla pagina iniziale.");
	    return;
	}

	if (duplicata) {
	    response.setStatus(473);
	    return;
	}

	try {
	    if (!utenteDAO.checkIdUtenti(utentiSelezionati)) {
		response.setStatus(471);
		return;
	    }
	} catch (SQLException e1) {
	    response.sendError(500, "Errore di connessione al database. Ritorna alla pagina iniziale.");
	    closeConnection(connection);
	    return;
	}

	// Inserisco la nuova riunione nel database
	int idRiunione = 0;
	try {
	    idRiunione = riunioneDAO.inserisciRiunione(titolo, dataeora, durata, numPartecipanti, user);
	} catch (SQLException e) {
	    response.sendError(500, "Errore di connessione al database. Ritorna alla pagina iniziale.");
	    closeConnection(connection);
	    return;
	}

	// Associo la nuova riunione agli utenti che sono stati invitati
	InvitatoDAO invitatoDAO = new InvitatoDAO(connection);
	for (Integer idUtenteSel : utentiSelezionati) {
	    try {
		invitatoDAO.associaUtente(idUtenteSel, idRiunione);
	    } catch (SQLException e) {
		response.sendError(500, "Errore di connessione al database. Ritorna alla pagina iniziale.");
		closeConnection(connection);
		return;
	    }
	}

	response.setStatus(200);
	closeConnection(connection);
    }

    private Connection openConnection() {
	Connection connection = null;
	boolean errDB = true;
	int tentativi = 0;
	while (errDB) {
	    if (connection == null) {
		try {
		    ServletContext context = getServletContext();
		    String driverDB = context.getInitParameter("dbDriver");
		    String urlDB = context.getInitParameter("dbUrl");
		    String userDB = context.getInitParameter("dbUser");
		    String passwordDB = context.getInitParameter("dbPassword");
		    Class.forName(driverDB);
		    connection = DriverManager.getConnection(urlDB, userDB, passwordDB);
		} catch (ClassNotFoundException | SQLException e) {
		    connection = null;
		    errDB = true;
		    if (++tentativi == 10) {
			return null;
		    }
		}
	    }
	    errDB = false;
	}
	return connection;
    }

    private void closeConnection(Connection connection) {
	try {
	    if (connection != null)
		connection.close();
	} catch (SQLException e) {
	    System.err.println("Error closing connection to database: " + e.toString());
	}
    }
}
