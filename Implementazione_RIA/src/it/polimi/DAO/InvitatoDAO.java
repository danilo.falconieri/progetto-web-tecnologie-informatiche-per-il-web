package it.polimi.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import it.polimi.beans.RiunioneBean;

public class InvitatoDAO {

	private Connection connection;

	public InvitatoDAO(Connection connection) {
		this.connection = connection;
	}

	public void associaUtente(int idUtente, int idRiunione) throws SQLException {
		String query = "INSERT INTO invitato(idUtente,idRiunione) VALUES (?,?)";

		PreparedStatement pstatement = null;
		try {
			pstatement = connection.prepareStatement(query);
			pstatement.setInt(1, idUtente);
			pstatement.setInt(2, idRiunione);
			pstatement.executeUpdate();
		} catch (SQLException e) {
			System.err.println("Errore esecuzione query in InvitatoDAO associaUtente" + e);
			throw e;
		} finally {
			try {
				if (pstatement != null)
					pstatement.close();
			} catch (Exception e1) {
				throw e1;
			}
		}
	}

	
	public List<RiunioneBean> getInvitiValidi(int idUtente) throws SQLException {
		List<RiunioneBean> inviti = new ArrayList<RiunioneBean>();
		String queryInvitato = "SELECT r.idRiunione, r.titolo, r.dataeora, r.durata, r.numPartecipanti, u.nome , u.cognome FROM (invitato AS i INNER JOIN riunione AS r ON i.idRiunione=r.idRiunione) INNER JOIN utente AS u ON r.organizzatore = u.idUtente WHERE i.idUtente = ? AND r.dataeora > ? ORDER BY r.dataeora ASC";
		String dataEoraAttuali = new SimpleDateFormat("yyyy-MM-dd HH:mm").format(new Date());

		ResultSet rs = null;
		PreparedStatement pstatement = null;
		try {
			pstatement = connection.prepareStatement(queryInvitato);
			pstatement.setInt(1, idUtente);
			pstatement.setString(2, dataEoraAttuali);
			rs = pstatement.executeQuery();
			while (rs.next()) {

				String organizzatore = rs.getString(6) + " " + rs.getString(7);
				String dataEora = "";
				String durata = "";
				try {
					dataEora = new SimpleDateFormat("dd-MM-yyyy HH:mm")
							.format(new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(rs.getString(3)));
					durata = new SimpleDateFormat("HH:mm")
							.format(new SimpleDateFormat("HH:mm:ss").parse(rs.getString(4)));
				} catch (ParseException e) {
					System.err.println("Error parsing string " + e.toString());
				}

				inviti.add(
						new RiunioneBean(rs.getInt(1), rs.getString(2), dataEora, durata, rs.getInt(5), organizzatore));
			}

		} catch (SQLException e) {
			System.err.println("Errore esecuzione query in InvitatoDAO getInvitiValidi" + e);
			throw e;
		} finally {
			try {
				if (rs != null)
					rs.close();
			} catch (Exception e1) {
				throw e1;
			}
			try {
				if (pstatement != null)
					pstatement.close();
			} catch (Exception e1) {
				throw e1;
			}
		}

		return inviti;
	}

}
