package it.polimi.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import it.polimi.beans.RiunioneBean;

public class RiunioneDAO {
	
	private Connection connection = null;
	
	public RiunioneDAO(Connection con) {
		this.connection = con;
	}

	// Metodo che inserisce una nuova riunione nel database. Prima interroga il database per determinare l'id da assegnare alla riunione, poi la inserisce
	public int inserisciRiunione(String titolo, String dataeora, String durata, int numPartecipanti, String organizzatore) throws SQLException {
		String queryIdRiunione = "SELECT MAX(idRiunione) FROM riunione";
		PreparedStatement prepStatement = null;
		PreparedStatement prepStatementUpdate = null;
		ResultSet rsRiunione = null;
		int idRiunione = 1;
		try {
			prepStatement = connection.prepareStatement(queryIdRiunione);
			rsRiunione = prepStatement.executeQuery();
			if(rsRiunione.next()) {
				idRiunione = rsRiunione.getInt(1);
				idRiunione++;
			}
			UtenteDAO utenteDAO = new UtenteDAO(connection);
			int idOrganizzatore = utenteDAO.getIdUtente(organizzatore);
			prepStatementUpdate = connection.prepareStatement("INSERT INTO riunione VALUES (?,?,?,?,?,?)");
			prepStatementUpdate.setInt(1, idRiunione);
			prepStatementUpdate.setString(2, titolo);
			prepStatementUpdate.setString(3, dataeora);
			prepStatementUpdate.setString(4, durata);
			prepStatementUpdate.setInt(5, numPartecipanti);
			prepStatementUpdate.setInt(6, idOrganizzatore);
			prepStatementUpdate.executeUpdate();
		} catch (SQLException e) {
			System.err.println("SQLException: " + e.toString());
			throw e;
		} finally {
			try {
				if(rsRiunione != null)
					rsRiunione.close();
				if(prepStatement != null)
					prepStatement.close();
				if(prepStatementUpdate != null)
					prepStatementUpdate.close();
			} catch (SQLException e) {
				System.err.println("SQLException: " + e.toString());
				throw e;
			}
		}
		return idRiunione;
	}
	
	
	// Metodo che restituisce le riunioni valide organizzate dall'utente
	public List<RiunioneBean> getRiunioniValideOrganizzate(String user) throws SQLException {
		UtenteDAO utenteDAO = new UtenteDAO(connection);
		int idUtente = utenteDAO.getIdUtente(user);
		PreparedStatement prepStatement = null;
		ResultSet rs = null;
		List<RiunioneBean> riunioniOrganizzate = new ArrayList<RiunioneBean>();
		try {
			prepStatement = connection.prepareStatement("SELECT titolo,dataeora,durata,numPartecipanti FROM riunione WHERE organizzatore = ? AND dataeora > ? ORDER BY dataeora ASC");
			prepStatement.setInt(1, idUtente);
			String dataEoraAttuali = new SimpleDateFormat("yyyy-MM-dd HH:mm").format(new Date());
			prepStatement.setString(2, dataEoraAttuali);
			rs = prepStatement.executeQuery();
			while(rs.next()) {
				RiunioneBean temp = new RiunioneBean();
				temp.setTitolo(rs.getString(1));
				try {
					temp.setDataeora(new SimpleDateFormat("dd-MM-yyyy HH:mm").format(new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(rs.getString(2))));
					temp.setDurata(new SimpleDateFormat("HH:mm").format(new SimpleDateFormat("HH:mm:ss").parse(rs.getString(3))));
				} catch (ParseException e) {
					System.err.println("Error parsing string: " + e.toString());
				}
				temp.setNumPartecipanti(rs.getInt(4));
				temp.setOrganizzatore(user);
				riunioniOrganizzate.add(temp);
			}
		} catch (SQLException e) {
			System.err.println("SQLException: " + e.toString());
			throw e;
		} finally {
			try {
				if(rs != null)
					rs.close();
				if(prepStatement != null)
					prepStatement.close();
			} catch (SQLException e) {
				System.err.println("SQLException: " + e.toString());
				throw e;
			}
		}
		
		return riunioniOrganizzate;
	}
	
	
	// Metodo per verificare che una riunione che si sta cercando di organizzare non sia fissata per la stessa data e ora di un'altra organizzata in precedenza
	public boolean verificaRiunioneDuplicata(String dataeora, String organizzatore) throws SQLException {
		String query = "SELECT * FROM riunione AS r INNER JOIN utente AS u ON r.organizzatore = u.idUtente WHERE r.dataeora = ? AND u.username = ?";
		PreparedStatement prepStatement = null;
		ResultSet rs = null;
		boolean duplicata = false;
		try {
			prepStatement = connection.prepareStatement(query);
			prepStatement.setString(1, dataeora);
			prepStatement.setString(2, organizzatore);
			rs = prepStatement.executeQuery();
			if(rs.next()) {
				duplicata = true;
			}
		} catch (SQLException e) {
			System.err.println("SQLException: " + e.toString());
			throw e;
		} finally {
			try {
				if(rs != null)
					rs.close();
				if(prepStatement != null)
					prepStatement.close();
			} catch (SQLException e) {
				System.err.println("SQLException: " + e.toString());
				throw e;
			}
		}
		return duplicata;
	}

}
