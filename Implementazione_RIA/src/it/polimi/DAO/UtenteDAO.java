package it.polimi.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import it.polimi.beans.UtenteBean;

public class UtenteDAO {
    private Connection connection = null;

    public UtenteDAO(Connection connection) {
	this.connection = connection;
    }

    // metodo che restituisce true se le credenziali di accesso sono corrette,
    // altrimenti false
    public boolean controlloUserPsw(String user, String psw) throws SQLException {
	boolean controllo = false;
	String query = "SELECT * FROM utente WHERE username = ? AND BINARY password = ?";
	ResultSet result = null;
	PreparedStatement pstatement = null;

	try {
	    pstatement = connection.prepareStatement(query);
	    pstatement.setString(1, user);
	    pstatement.setString(2, psw);
	    result = pstatement.executeQuery();
	    if (result.next()) {
		controllo = true;
	    }
	} catch (SQLException e) {
	    System.err.println("Errore query UtenteDAO, controlloUserPsw" + e);
	    throw e;
	} finally {
	    try {
		if (result != null)
		    result.close();
	    } catch (Exception e1) {
		throw e1;
	    }
	    try {
		if (pstatement != null)
		    pstatement.close();
	    } catch (Exception e1) {
		throw e1;
	    }
	}

	return controllo;
    }

    // metodo che restitui lista di tutti i dati degli utente nel database, eccetto
    // lo user passato come parametro
    public List<UtenteBean> getAllDataWithoutPsw(String user) throws SQLException {
	List<UtenteBean> utenti = new ArrayList<UtenteBean>();
	String query = "SELECT idUtente, username, nome, cognome FROM utente WHERE username != ?";
	ResultSet result = null;
	PreparedStatement pstatement = null;

	try {
	    pstatement = connection.prepareStatement(query);
	    pstatement.setString(1, user);
	    result = pstatement.executeQuery();
	    while (result.next()) {
		UtenteBean utente = new UtenteBean();
		utente.setIdUtente(result.getInt("idUtente"));
		utente.setUsername(result.getString("username"));
		utente.setNome(result.getString("nome"));
		utente.setCognome(result.getString("cognome"));
		utenti.add(utente);
	    }
	} catch (SQLException e) {
	    System.err.println("Errore esecuzione query in UtenteDAO getAllDataWithoutPsw" + e);
	    throw e;
	} finally {
	    try {
		if (result != null)
		    result.close();
	    } catch (Exception e1) {
		throw e1;
	    }
	    try {
		if (pstatement != null)
		    pstatement.close();
	    } catch (Exception e1) {
		throw e1;
	    }
	}

	return utenti;
    }

    // restituisce l'idUtente dato lo username
    public int getIdUtente(String user) throws SQLException {
	String queryIdUser = "SELECT idUtente FROM utente WHERE username = ?";
	PreparedStatement pstatement = null;
	ResultSet result = null;
	int idUser = 0;
	try {
	    pstatement = connection.prepareStatement(queryIdUser);
	    pstatement.setString(1, user);
	    result = pstatement.executeQuery();
	    if (result.next()) {
		idUser = result.getInt(1);
	    }
	} catch (SQLException e) {
	    System.err.println("Errore esecuzione query in UtenteDAO getIdUtente" + e);
	    throw e;
	} finally {
	    try {
		if (result != null)
		    result.close();
	    } catch (Exception e1) {
		throw e1;
	    }
	    try {
		if (pstatement != null)
		    pstatement.close();
	    } catch (Exception e1) {
		throw e1;
	    }
	}

	return idUser;
    }

    public boolean checkMail(String mail) throws SQLException {
	String query = "SELECT * FROM utente WHERE mail = ?";
	PreparedStatement pStatement = null;
	ResultSet res = null;
	try {
	    pStatement = connection.prepareStatement(query);
	    pStatement.setString(1, mail);
	    res = pStatement.executeQuery();
	    if (res.next()) {
		return false;
	    } else {
		return true;
	    }
	} catch (SQLException e) {
	    System.err.println("Errore esecuzione query in UtenteDAO checkMail" + e);
	    throw e;
	} finally {
	    try {
		if (res != null)
		    res.close();
	    } catch (Exception e1) {
		throw e1;
	    }
	    try {
		if (pStatement != null)
		    pStatement.close();
	    } catch (Exception e1) {
		throw e1;
	    }
	}
    }

    public boolean checkUser(String username) throws SQLException {
	String query = "SELECT * FROM utente WHERE username = ?";
	PreparedStatement pStatement = null;
	ResultSet res = null;
	try {
	    pStatement = connection.prepareStatement(query);
	    pStatement.setString(1, username);
	    res = pStatement.executeQuery();
	    if (res.next()) {
		return false;
	    } else {
		return true;
	    }
	} catch (SQLException e) {
	    System.err.println("Errore esecuzione query in UtenteDAO checkUser" + e);
	    throw e;
	} finally {
	    try {
		if (res != null)
		    res.close();
	    } catch (Exception e1) {
		throw e1;
	    }
	    try {
		if (pStatement != null)
		    pStatement.close();
	    } catch (Exception e1) {
		throw e1;
	    }
	}
    }

    public boolean insertUser(String mail, String username, String nome, String cognome, String password)
	    throws SQLException {
	String query = "INSERT INTO utente (mail,username,nome,cognome,password) VALUES (?,?,?,?,?)";
	PreparedStatement pStatement = null;
	int res = 0;
	try {
	    pStatement = connection.prepareStatement(query);
	    pStatement.setString(1, mail);
	    pStatement.setString(2, username);
	    pStatement.setString(3, nome);
	    pStatement.setString(4, cognome);
	    pStatement.setString(5, password);
	    res = pStatement.executeUpdate();
	    if (res == 0) {
		return false;
	    } else {
		return true;
	    }
	} catch (SQLException e) {
	    System.err.println("Errore esecuzione query in UtenteDAO insertUser" + e);
	    throw e;
	} finally {
	    try {
		if (pStatement != null)
		    pStatement.close();
	    } catch (Exception e1) {
		throw e1;
	    }
	}
    }

    // data lista di utenti verifico se ogni utente in lista, � presente nel DB
    public boolean checkIdUtenti(ArrayList<Integer> listaUtenti) throws SQLException {
	String query = "SELECT username FROM utente WHERE idUtente = ?";
	PreparedStatement pStatement = null;
	ResultSet rs = null;
	boolean risultato = true;
	try {
	    pStatement = connection.prepareStatement(query);
	    for (Integer utente : listaUtenti) {
		pStatement.setInt(1, utente);
		rs = pStatement.executeQuery();
		if (!rs.next()) {
		    risultato = false;
		    break;
		}
	    }
	} catch (SQLException e) {
	    System.err.println("Errore esecuzione quert in UtenteDAO checkIdUtenti" + e);
	    throw e;
	} finally {
	    try {
		if (rs != null)
		    rs.close();
	    } catch (Exception e1) {
		throw e1;
	    }
	    try {
		if (pStatement != null)
		    pStatement.close();
	    } catch (Exception e1) {
		throw e1;
	    }
	}
	return risultato;
    }

    public boolean checkIdUtenteUsername(int idUtente, String username) throws SQLException {
	boolean associati = false;

	try {
	    if (getIdUtente(username) == idUtente) {
		associati = true;
	    }
	} catch (SQLException e) {
	    throw e;
	}
	return associati;
    }

}
