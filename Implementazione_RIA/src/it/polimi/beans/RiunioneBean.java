package it.polimi.beans;

public class RiunioneBean {
	
	private int idRiunione;
	private String titolo;
	private String dataeora;
	private String durata;
	private int numPartecipanti;
	private String organizzatore;
	
	public RiunioneBean() {
		idRiunione = 0;
		titolo = "";
		dataeora = "";
		durata = "";
		numPartecipanti = 0;
		organizzatore = "";
	}
	
	public RiunioneBean(int id, String titolo, String dataeora, String durata, int numPart, String organizzatore) {
		this.idRiunione = id;
		this.titolo = titolo;
		this.dataeora = dataeora;
		this.durata = durata;
		this.numPartecipanti = numPart;
		this.organizzatore = organizzatore;
	}
	
	public int getIdRiunione() {
		return idRiunione;
	}
	
	public void setIdRiunione(int idRiunione) {
		this.idRiunione = idRiunione;
	}
	
	public String getTitolo() {
		return titolo;
	}
	
	public void setTitolo(String titolo) {
		this.titolo = titolo;
	}
	
	public String getDataeora() {
		return dataeora;
	}
	
	public void setDataeora(String dataeora) {
		this.dataeora = dataeora;
	}
	
	public String getDurata() {
		return durata;
	}
	
	public void setDurata(String durata) {
		this.durata = durata;
	}
	
	public int getNumPartecipanti() {
		return numPartecipanti;
	}
	
	public void setNumPartecipanti(int numPartecipanti) {
		this.numPartecipanti = numPartecipanti;
	}
	
	public String getOrganizzatore() {
		return organizzatore;
	}
	
	public void setOrganizzatore(String organizzatore) {
		this.organizzatore = organizzatore;
	}
	
}
