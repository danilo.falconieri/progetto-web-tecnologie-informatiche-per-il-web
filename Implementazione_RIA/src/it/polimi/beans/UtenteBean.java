package it.polimi.beans;

public class UtenteBean {
	
	private int idUtente;
	private String username;
	private String nome;
	private String cognome;
	
	public UtenteBean() {
		idUtente = 0;
		username = "";
		nome = "";
		cognome = "";
	}
	
	public UtenteBean(int pIdUtente, String pUsername, String pNome, String pCognome) {
		this.idUtente = pIdUtente;
		this.username = pUsername;
		this.nome = pNome;
		this.cognome = pCognome;
	}
	
	public void setIdUtente(int idUtente) {
		this.idUtente = idUtente;
	}
	
	public int getIdUtente() {
		return idUtente;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getUsername() {
		return username;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String getNome() {
		return nome;
	}
	
	public void setCognome(String cognome) {
		this.cognome = cognome;
	}
	
	public String getCognome() {
		return cognome;
	}
	
}