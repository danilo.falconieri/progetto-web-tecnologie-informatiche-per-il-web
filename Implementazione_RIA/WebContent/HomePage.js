(function() {
	/**
	 * Funzioni javascript pagina HomePage.html
	 */

	window.onload = function() {
		if (sessionStorage.getItem('username') == null)
			window.location.replace("Index.html");
		document.getElementById("benvenuto").textContent = sessionStorage
				.getItem('username')
				+ " |";
		loadRiunioni();


		// DISABILITO LE DATE PASSATE
		var dtToday = new Date();
		var month = dtToday.getMonth() + 1;
		if (dtToday.getHours() >= 16) {
			var day = dtToday.getDate() + 1;
		} else {
			var day = dtToday.getDate();
		}
		var year = dtToday.getFullYear();
		if (month < 10)
			month = '0' + month.toString();
		if (day < 10)
			day = '0' + day.toString();
		var maxDate = year + '-' + month + '-' + day;
		document.getElementById("data").setAttribute('min', maxDate);
		// FINE - DISABILITO LE DATE PASSATE

		closeAnagrafica();
	}

	// funzione che disabilita un qualsisi pulsante
	function disableBtn(btn) {
		btn.disabled = true;
		btn.style.color = "grey";
		btn.style.backgroundColor = "lightgrey";
		btn.style.boxShadow = "none";
	}
	// funzione che abilita qualsiasi pulsante
	function enableBtn(btn) {
		btn.disabled = false;
		btn.style.color = "black";
		btn.style.backgroundColor = "white";
	}
	// funzioni di stle per pulsanti
	function mouseOverStyle(btn) {
		btn.style.backgroundColor = "#004d3d";
		btn.style.color = "white";
		btn.style.boxShadow = "0 12px 16px 0 rgba(0, 0, 0, 0.24), 0 17px 50px 0 rgba(0, 0, 0, 0.19)";
	}

	function mouseOutStyle(btn) {
		btn.style.backgroundColor = "white";
		btn.style.color = "black";
		btn.style.boxShadow = "none";
	}

	function loadRiunioni() {
		if (sessionStorage.getItem('username') == null)
			window.location.replace("Index.html");

		window.scrollTo({
			top : 0,
			behavior : 'smooth'
		});

		document.getElementById("newRiunione").reset();

		enableBtn(document.getElementById("btnInvia"));

		// azzero le tabelle delle riunioni
		var tOrganizzate = document.querySelector("#organizzate tbody");
		var tInviti = document.querySelector("#inviti tbody");
		while (tOrganizzate.rows.length != 0) {
			tOrganizzate.deleteRow(0);
		}
		while (tInviti.rows.length != 0) {
			tInviti.deleteRow(0);
		}

		var xhttp = new XMLHttpRequest();
		xhttp.onreadystatechange = function() {
			if (this.readyState == 4) {
				switch (this.status) {
				case 200:
					var listeRiunioni = JSON.parse(this.responseText);
					var riunioniOrganizzate = listeRiunioni[0];
					var riunioniInvitato = listeRiunioni[1];

					// Inserisco le riunioni organizzate nella tabella
					// corrispondente
					for (var i = 0; i < riunioniOrganizzate.length; i++) {
						// creo una nuova riga
						let newRow = tOrganizzate
								.insertRow(tOrganizzate.length);
						let cellTitolo = newRow.insertCell(0);
						cellTitolo.innerHTML = riunioniOrganizzate[i].titolo;
						let cellDataOra = newRow.insertCell(1);
						cellDataOra.innerHTML = riunioniOrganizzate[i].dataeora;
						let cellDurata = newRow.insertCell(2);
						cellDurata.innerHTML = riunioniOrganizzate[i].durata;
						let cellNumPartecipanti = newRow.insertCell(3);
						cellNumPartecipanti.innerHTML = riunioniOrganizzate[i].numPartecipanti;
					}

					// Inserisco gli inviti nella tabella corrispondente
					for (var i = 0; i < riunioniInvitato.length; i++) {
						// creo una nuova riga
						let newRow = tInviti.insertRow(tInviti.length);
						let cellTitolo = newRow.insertCell(0);
						cellTitolo.innerHTML = riunioniInvitato[i].titolo;
						let cellDataOra = newRow.insertCell(1);
						cellDataOra.innerHTML = riunioniInvitato[i].dataeora;
						let cellDurata = newRow.insertCell(2);
						cellDurata.innerHTML = riunioniInvitato[i].durata;
						let cellNumPartecipanti = newRow.insertCell(3);
						cellNumPartecipanti.innerHTML = riunioniInvitato[i].numPartecipanti;
						let cellOrganizzatore = newRow.insertCell(4);
						cellOrganizzatore.innerHTML = riunioniInvitato[i].organizzatore;
					}
					break;
				case 777:
					var url = this.getResponseHeader("Location");
					window.location.replace(url);
					break;
				default:
					printErrorPage(this);
				}
			}
		}

		xhttp.open("GET", "GetRiunioni", true);
		xhttp.send();
	}

	// LOGOUT
	var btnLogout = document.getElementById("btnLogout");
	btnLogout.addEventListener("click", effettuaLogout, false);
	function effettuaLogout() {
		sessionStorage.removeItem('username');
		var xhttp = new XMLHttpRequest();
		xhttp.onreadystatechange = function() {
			if (this.readyState == 4) {
				switch (this.status) {
				case 200:
					var url = this.getResponseHeader("Location");
					window.location.replace(url);
					break;
				default:
					printErrorPage(this);
				}
			}
		}
		xhttp.open("GET", "Logout", true);
		xhttp.send();
	}
	// FINE - LOGOUT

	// Gestisco il tasco X di cancellazione
	const closeBtn = document.querySelector('.close');
	closeBtn.addEventListener('click', tornaAllaHome);

/*
 * sezione di codice della pagina modale Anagrafica
 */
	// funzione che stampa a video un errore nella sezione predisposta di anagrafica
	function showErrorAnagrafica(err) {
		var error = document.getElementById("error");
		error.innerHTML = err;
		error.style.display = "block";
	}

	// funziona che si occupa di mostrare il messaggio di errore relativo ai
	// tentativi, in base ai parametri passati
	function showTentativiErrorAngrafica(nSelezionati, numPartecipanti,
			nTentativi) {
		document.getElementById("tentativiRimanenti").innerHTML = "";
		var tentativiRimasti = 4 - nTentativi;
		var strTentativi = "";
		if (tentativiRimasti > 1) {
			strTentativi = " tentativi.";
		} else {
			strTentativi = " tentativo.";
		}
		var nUtentiDaRimuovere = nSelezionati - numPartecipanti;
		if (nUtentiDaRimuovere >= 0) {
			var strDaRimuovere = "";
			if (nUtentiDaRimuovere > 1) {
				strDaRimuovere = " utenti.";
			} else {
				strDaRimuovere = " utente.";
			}
			if (tentativiRimasti != 3)
				showErrorAnagrafica("Numero di persone selzionate maggiore del numero massimo scelto!<br />Ti restano "
						+ tentativiRimasti
						+ strTentativi
						+ "<br />Devi deselezionare "
						+ nUtentiDaRimuovere
						+ strDaRimuovere);
		}
	}

	// aggiungo listener click per btnInvia
	var btnInvia = document.getElementById("btnInvia");
	btnInvia.addEventListener("click", effettuaControlliInvia, false);

	function effettuaControlliInvia() {
		//perchè reportValidity abbinato al custom effettuava il refresh della pagina 
		event.preventDefault();
		
		var inputOra = document
				.querySelector("form#newRiunione input[name='ora']");
		//resetto il valore del messaggio di reportValidity
		inputOra.setCustomValidity("");

		// verifico che la data inserita per la riunione non sia passata. Prelevo dal campo data della nuova riunione
		// i valori del giorno, mese, anno e li confronto con giorno, mese, anno di oggi. Se le due date coincdono cioè
		// sto cercando di inserire una riunione per oggi stesso verifico l'ora. Se l'ora è nel passato mostro l'errore.
		var dataPassata = document
				.querySelector("form#newRiunione input[name='data']").value;
		var aDataPassata = dataPassata.split("-");
		var actualDate = new Date();
		var actualDay = actualDate.getDate();
		var actualMonth = actualDate.getMonth() + 1;
		var actualYear = actualDate.getFullYear();

		if (aDataPassata[0] == actualYear && aDataPassata[1] == actualMonth
				&& aDataPassata[2] == actualDay) {
			var oraPassata = document
					.querySelector("form#newRiunione input[name='ora']").value;
			var aOraPassata = oraPassata.split(":");

			var actualHour = actualDate.getHours();
			var actualMinutes = actualDate.getMinutes();
			if (aOraPassata[0] <= actualHour && aOraPassata[1] <= actualMinutes) {
				//setto il valore del messaggio di reportValidity e lo mostro
				inputOra
						.setCustomValidity("Impossibile organizzare riunioni nel passato");
				inputOra.reportValidity();

				return;
			}
		}

		//controllo il form
		if (document.getElementById("newRiunione").reportValidity()) {
			showAnagrafica();
		}
	}

	btnInvia.addEventListener("mouseover", function() {
		mouseOverStyle(btnInvia);
	}, false);
	btnInvia.addEventListener("mouseout", function() {
		mouseOutStyle(btnInvia);
	}, false);

	function showAnagrafica() {
		// disabilito pulsante invia, evito doppio click
		disableBtn(btnInvia);
		// resetto il form di selezione utenti da invitare
		cancelDataAnagrafica();
		// disabilito pulsante invita, che viene abilitato solo se ho selezionato almeno una checkbox
		disableBtn(document.getElementById("btnInvita"));
		// disabilito scrollbar pagina principale
		document.body.style.overflowY = "hidden";

		var xhttp;
		window.sessionStorage.setItem("nTentativi", 1);

		if (window.XMLHttpRequest)
			xhttp = new XMLHttpRequest();
		else
			xhttp = new ActiveXObject("Microsoft.XMLHTTP");

		xhttp.onreadystatechange = function() {
			if (this.readyState == XMLHttpRequest.DONE) {
				switch (this.status) {
				case 200:
					// prelevo la lista degli utenti
					var listaUtenti = JSON.parse(this.responseText);
					var bodyFormInvito = document
							.querySelector("form#formInvito table tbody");

					// creo la tabella di selezione utenti
					listaUtenti
							.forEach(function(utente) {
								var tr = document.createElement("tr");
								var tdNome = document.createElement("td");
								var tdCognome = document.createElement("td");
								var tdUser = document.createElement("td");
								var tdCheckBox = document.createElement("td");
								var checkBox = document.createElement("input");
								checkBox.type = "checkbox";
								checkBox.className = "formSelezione";
								checkBox.value = utente.idUtente + ";"
										+ utente.username;

								// aggiungo listener alle checkbox che si occupa di: 
								// - aggiornare il messaggio di errore se questo era stato visualizzato (se sono oltre il primo tentativo)
								// - abilitare il pulsante invita se alemno una checkbox è selezionata
								checkBox
										.addEventListener(
												"click",
												function() {
													var selezionati = document
															.querySelectorAll(".formSelezione");
													var numPartecipanti = document
															.querySelector("form#newRiunione input[name='numPartecipanti']").value;
													var error = document
															.getElementById("error");
													if (error.style.display == "block") {
														// contare il numero di
														// checkbox selezionate
														// e stampo il messaggio
														var nSelezionati = 0;
														selezionati
																.forEach(function(
																		selezionato) {
																	if (selezionato.checked == true) {
																		nSelezionati += 1;
																	}
																});

														showTentativiErrorAngrafica(
																nSelezionati,
																numPartecipanti,
																window.sessionStorage
																		.getItem("nTentativi"));
													}

													// riattivo btnInvita se
													// almeno una checkbox è
													// stata
													// selezionata
													var trueFind = false;
													selezionati
															.forEach(function(
																	selezionato) {
																if (!trueFind) {
																	if (selezionato.checked === true) {
																		enableBtn(btnInvita);
																		trueFind = true;
																		return;
																	}
																	disableBtn(btnInvita);
																}
															});
												}, false);
								// aggiungo contenuto dinamico alla tabella
								tdNome.textContent = utente.nome;
								tdCognome.textContent = utente.cognome;
								tdUser.textContent = utente.username;

								tr.appendChild(tdNome);
								tr.appendChild(tdCognome);
								tr.appendChild(tdUser);
								tdCheckBox.appendChild(checkBox);
								tr.appendChild(tdCheckBox);
								bodyFormInvito.appendChild(tr);
							});

					// modifico il titolo con il numero corretto di maxUtenti selezionabili
					var maxUtenti = document
							.querySelector("form#newRiunione input[name='numPartecipanti']").value;
					document.querySelector("#maxUtenti").innerHTML = "Seleziona al massimo <strong>"
							+ maxUtenti
							+ "</strong> utenti da invitare alla riunione."

					// mostra anagrafica
					document.getElementById("paginaModale").style.display = "block";
					break;
				case 777:
					// se mi è stata ordinato una redirect, faccio la redirect
					var url = this.getResponseHeader("Location");
					window.location.replace(url)
					break;
				default:
					printErrorPage(this);
				}
			}
		}
		xhttp.open("GET", "GetDatiAnagrafica", true);
		xhttp.send();
	}

	// aggiungo listener click per btnInvita
	var btnInvita = document.getElementById("btnInvita");
	btnInvita.addEventListener("click", invitaSelezionati, false);

	function invitaSelezionati() {
		// controllo nuovamente i dati nuova riunione in modo da impedire le modifiche che un utente malizioso potrebbe fare dopo
		// aver aperto anagrafica
		if (!document.getElementById("newRiunione").checkValidity()) {
			alert("Dati nuova riunione non validi");
			closeAnagrafica();
			return;
		}
		
		// rieffettuo il controllo sulla data che non sia nel passato come effettuaControlliInvia()
		var dataPassata = document
				.querySelector("form#newRiunione input[name='data']").value;
		var aDataPassata = dataPassata.split("-");
		var actualDate = new Date();
		var actualDay = actualDate.getDate();
		var actualMonth = actualDate.getMonth() + 1;
		var actualYear = actualDate.getFullYear();

		if (aDataPassata[0] == actualYear && aDataPassata[1] == actualMonth
				&& aDataPassata[2] == actualDay) {
			var oraPassata = document
					.querySelector("form#newRiunione input[name='ora']").value;
			var aOraPassata = oraPassata.split(":");

			var actualHour = actualDate.getHours();
			var actualMinutes = actualDate.getMinutes();
			if (aOraPassata[0] <= actualHour && aOraPassata[1] <= actualMinutes) {
				alert("Impossibile organizzare riunioni nel passato");
				return;
			}
		}

		// ricavo numero selezionati dall'utente e gli nTentativi dalla sessione
		var selezionati = document.querySelectorAll(".formSelezione");
		var nSelezionati = 0;
		var params = "";
		var nTentativi = window.sessionStorage.getItem("nTentativi");

		// controllo che l'utente non abbia selezionato due volte lo stesso
		// utente
		// ciò capita quando viene alterata una checkbox inserendo un value diverso
		for (var i = 0; i < selezionati.length; i++) {
			for (var j = 0; j < selezionati.length; j++) {
				if (selezionati[i].value === selezionati[j].value && i !== j) {
					alert("Errore dati nella lista di utenti da invitare, invitato duplicato");
					closeAnagrafica();
					return;
				}
			}
		}

		//aggiungo ai parametri da inviare con la richiesta il campo value delle checkbox selezionate
		selezionati.forEach(function(selezionato) {
			if (selezionato.checked === true) {
				nSelezionati++;
				params += "selezionati=" + selezionato.value + "&";
			}
		});

		
		//controllo che ci sia almeno un selezionato, in caso mostro errore ma non conto il tentativo
		if (nSelezionati === 0) {
			showErrorAnagrafica("Selezionare almeno un invitato!");
			return;
		}

		// prelevo il massimo numero di utenti selezionavili e faccio il confronto con il numero di selezioanti
		// se è minore o uguale allora procedo con la richiesta
		// se è maggiore e mi restano ancora dei tentatvi incremento tentativi e mostro errore
		// se è maggiore e non mi restano tentativi showCancellazione
		var numPartecipanti = document
				.querySelector("form#newRiunione input[name='numPartecipanti']").value;
		if (nSelezionati <= numPartecipanti) {
			var xhttp;

			if (window.XMLHttpRequest)
				xhttp = new XMLHttpRequest();
			else
				xhttp = new ActiveXObject("Microsoft.XMLHTTP");

			xhttp.onreadystatechange = function() {
				if (this.readyState == 4) {
					switch (this.status) {
					case 200:
						loadRiunioni();
						closeAnagrafica();
						break;
					case 470:
						alert("Errore dati nuova riunione");
						closeAnagrafica();
						break;
					case 471:
						alert("Errore dati nella lista di utenti da invitare");
						closeAnagrafica();
						break;
					case 472:
						alert("Numero massimo di tentativi superato");
						showCancellazione();
						break;
					case 473:
						alert("Riunione Duplicata");
						closeAnagrafica();
						break;
					case 777: // se mi è stata ordinato una redirect, faccio
						// la redirect
						var url = this.getResponseHeader("Location");
						window.location.replace(url)
						break;
					default:
						printErrorPage(this);
					}
				}
			}

			// Prelevo i parametri da passare: titolo, dataeora, durata,
			// numPartecipanti, selezionati, nTentativi
			var titolo = document
					.querySelector("form#newRiunione input[name='titolo']").value;
			var dataeora = document
					.querySelector("form#newRiunione input[name='data']").value
					+ " "
					+ document
							.querySelector("form#newRiunione input[name='ora']").value;
			var durata = document
					.querySelector("form#newRiunione input[name='durata']").value;
			params += "titolo=" + titolo + "&dataeora=" + dataeora + "&durata="
					+ durata + "&numPartecipanti=" + nSelezionati
					+ "&nTentativi=" + nTentativi;

			xhttp.open("POST", "AggiornaDB", true);
			xhttp.setRequestHeader("Content-type",
					"application/x-www-form-urlencoded");
			xhttp.send(params);
			// se il numero di invitati è maggiore del massimo numero di persone
			// invitabili e ho già fatto 3 tentativi
		} else if (nSelezionati > numPartecipanti && nTentativi == 3) {
			showCancellazione();
			// se il numero di invitati è maggiore del massimo numero di persone
			// invitabili e non ho fatto 3 tentativi
		} else if (nSelezionati > numPartecipanti && nTentativi < 3) {
			// aumento nTentativi
			window.sessionStorage.setItem("nTentativi", ++nTentativi);
			// stampo a video il messaggio dei tentativi
			showTentativiErrorAngrafica(nSelezionati, numPartecipanti,
					nTentativi);
		}
	}

	// aggiungo proprietà di stile all'invita btnInvita
	btnInvita.addEventListener("mouseover", function() {
		mouseOverStyle(btnInvita);
	}, false);
	btnInvita.addEventListener("mouseout", function() {
		mouseOutStyle(btnInvita);
	}, false);

	// aggiungo listener click per btnCancella
	var btnCancella = document.getElementById("btnCancella");
	btnCancella.addEventListener("click", closeAnagrafica, false);

	function closeAnagrafica() {
		// riabilito pulsante invia
		enableBtn(document.getElementById("btnInvia"));
		// riabilito scrollbar della pagina principale
		document.body.style.overflowY = "visible";

		// rimuove messaggi errore
		document.getElementById("error").style.display = "none";
		// chiude la pagina
		document.getElementById("paginaModale").style.display = "none";

		window.sessionStorage.setItem("nTentativi", 1);
	}

	function cancelDataAnagrafica() {
		// rimuovo le tbody tr della pagina modale
		var bodyFormInvito = document
				.querySelectorAll("form#formInvito table tbody tr");
		bodyFormInvito.forEach(function(tr) {
			tr.remove();
		});
	}

	// gestisco eventi di in e out del mouse per cambiare background alla X
	// button
	var spanClose = document.querySelector("div#title span");
	spanClose.addEventListener("click", closeAnagrafica, false);

/*
 * Funzioni di Cancellazione
 */
	// mostra la pagina modale di cancellazione
	function showCancellazione() {
		document.querySelector('#blurDiv').style.filter = 'blur(5px)';
		document.querySelector('.toolBar').style.filter = 'blur(5px)';
		document.querySelector('#myVideo').style.filter = 'blur(5px)';
		closeAnagrafica();
		document.getElementById("cancellazione").style.display = "block";
	}

	var btnTornaAllaHome = document.getElementById("btnTornaAllaHome");
	btnTornaAllaHome.addEventListener("click", tornaAllaHome, false);

	function tornaAllaHome() {
		loadRiunioni();
		closeAnagrafica();
		document.querySelector('.toolBar').style.filter = 'blur(0px)';
		document.querySelector('#myVideo').style.filter = 'blur(0px)';
		document.querySelector('#blurDiv').style.filter = 'blur(0px)';
		document.getElementById("cancellazione").style.display = "none";
	}

})();