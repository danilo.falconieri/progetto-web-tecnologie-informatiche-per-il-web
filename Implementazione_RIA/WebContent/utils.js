/**
 * funzioni di utilità
 */

function printErrorPage(request){	
	document.write(request.responseText);
	var a = document.createElement("a");
	a.innerHTML = "Torna alla pagina iniziale";
	a.setAttribute("href", "Index.html");
	document.body.appendChild(a);
}

function makeCall(method, url, username, password, cback) {
	var req = new XMLHttpRequest(); // visible by closure
	req.onreadystatechange = function() {
		cback(req);
	}; // closure
	var params = "username=" + username + "&password=" + password;
	req.open(method, url, true);
	req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	req.send(params);
}