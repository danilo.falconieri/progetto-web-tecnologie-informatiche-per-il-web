/**
 * funioni javascript per il login
 */

(function() {
	document.getElementById("LoginButton").addEventListener('click', check);

	addEventListener('keypress', function(e) {
		if (e.key === 'Enter') {
			if(document.getElementById("ModalRegistrazione").offsetWidth == 0 && document.getElementById("ModalRegistrazione").offsetHeight == 0 && document.getElementById("messaggioSuccesso").offsetWidth == 0 && document.getElementById("messaggioSuccesso").offsetHeight == 0){
				check();
			}
		}
	});
	
	function check() {
		document.getElementById("errormessage").textContent = "";
		
		var form = document.getElementById("LoginForm");
		var username = form[0].value;
		var password = form[1].value;
		
		if(!form.reportValidity())
			return;
			
		makeCall("POST", 'CheckLogin', username, password, function(req) {

			if (req.readyState == XMLHttpRequest.DONE) {
				var message = req.responseText;

				switch (req.status) {
				case 200:
					sessionStorage.setItem('username', username);
					window.location.href = "HomePage.html";
					break;
				case 400: // bad request
					document.getElementById("errormessage").textContent = message;
					break;
				case 470: // username || password == null
					document.getElementById("errormessage").textContent = message;
					break;
				case 471: // username error
					document.getElementById("errormessage").textContent = message;
					break;
				default:
					printErrorPage(req);
				}
			}
		});
	}
})();