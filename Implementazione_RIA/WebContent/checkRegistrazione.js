/**
 * funzioni javascript per il controllo della registrazione di un nuovo utente
 */

(function (){
	
	// Funzione che mostra la pagina modale per la registrazione di un nuovo utente
	document.getElementById("SignUpButton").addEventListener("click",ShowRegistrazioneForm);
	function ShowRegistrazioneForm(){
		for(var i=0; i<document.getElementsByClassName("errorRegistrazione").length; i++){
			document.getElementsByClassName("errorRegistrazione")[i].style.display = "none";
		}
		document.getElementById("ModalRegistrazione").style.display = "block";
		document.getElementById("LoginForm").reset();
		document.getElementById("errormessage").textContent = "";
	}
	
	// Il cursore viene impostato sul valore di default per il pulsante della registrazione, se questo è disattivato
	if(document.getElementById("RegistrazioneButton").disabled === true){
		document.getElementById("RegistrazioneButton").style.cursor = "default";
	}
	
	// Funzione per verificare che la mail inserita sia sintatticamente valida (errore[0] ed errore[1]) e disponibile (errore[2])
	document.getElementById("mail").addEventListener("focusout",checkEmail);
	function checkEmail(async = true){
		// Se il pulsante per registrarsi è attivo, la chiamata AJAX è sincrona, altrimenti c'è il rischio di inserire una mail già presente nel database
		if(document.getElementById("RegistrazioneButton").disabled === false){
			async = false;
		}
		
		// Controllo se il dato inserito è nullo, nel caso mostro un messaggio d'errore
		if(document.getElementById("mail").value === ""){
			document.getElementsByClassName("errorRegistrazione")[0].style.display = "block";
			document.getElementsByClassName("errorRegistrazione")[1].style.display = "none";
			return;
		} else {
			document.getElementsByClassName("errorRegistrazione")[0].style.display = "none";
		}
		
		// Se il campo mail risulta convalidato, eseguo la chiamata AJAX per verificare la disponibilità della mail inserita. Nel caso non lo sia, mostro un messaggio d'errore
		// Nel caso il campo mail non sia convalidato , mostro un messaggio d'errore
		if(document.getElementById("mail").checkValidity() === false){
			document.getElementsByClassName("errorRegistrazione")[2].style.display = "none";
			document.getElementsByClassName("errorRegistrazione")[1].style.display = "block";
			activeSubmitRegistrazione();
			return;
		} else {
			document.getElementById("mail").classList.add("loading");
			document.getElementsByClassName("errorRegistrazione")[1].style.display = "none";
			var mail = document.getElementById("mail").value;
			var xhml = new XMLHttpRequest();
			xhml.onreadystatechange = function(){
				if(xhml.readyState == 4){
					switch(xhml.status){
						case 470:
							document.getElementById("mail").classList.remove("loading");
							document.getElementsByClassName("errorRegistrazione")[2].style.display = "block";
							// Verifico se bisogna attivare/disattivare il pulsante di registrazione
							activeSubmitRegistrazione();
							break;				 
						case 200:
							document.getElementById("mail").classList.remove("loading");
							document.getElementsByClassName("errorRegistrazione")[2].style.display = "none";
							// Verifico se bisogna attivare/disattivare il pulsante di registrazione
							activeSubmitRegistrazione();
							break;				
						case 472:
							// mostro l'errore 11, diabilita il pulsante annulla e attendo 8 secondi e richiamo annullaRegistrazione
							document.getElementsByClassName("errorRegistrazione")[11].style.display = "block";
							document.getElementById("AnnullaRegistrazione").disabled = true;
							document.getElementById("AnnullaRegistrazione").style.cursor = "default";
							setTimeout(annullaRegistrazione, 8000);
							break;					
						default:
							// mostro l'errore 12, diabilita il pulsante annulla e attendo 8 secondi e richiamo annullaRegistrazione
							document.getElementsByClassName("errorRegistrazione")[12].innerHTML = "Error " + xhml.status + ". A breve verrai reindirizzato alla pagina iniziale";
							document.getElementsByClassName("errorRegistrazione")[12].style.display = "block";
							document.getElementById("AnnullaRegistrazione").disabled = true;
							document.getElementById("AnnullaRegistrazione").style.cursor = "default";
							setTimeout(annullaRegistrazione, 8000);
					}
				}
			};
			xhml.open("GET","CheckEmail?mail=" + mail, Boolean(async));
			xhml.send(); 
		}		
	}
	
	// Funzione per verificare che l'username inserito sia valido e disponibile
	document.getElementById("username").addEventListener("focusout",checkUser);
	function checkUser(async = true){
		// Se il pulsante per registrarsi è attivo, la chiamata AJAX è sincrona, altrimenti c'è il rischio di inserire un username già presente nel database
		if(document.getElementById("RegistrazioneButton").disabled === false){
			async = false;
		}
		
		// Controllo se il dato inserito è nullo, nel caso mostro un messaggio d'errore
		if(document.getElementById("username").value === ""){
			document.getElementsByClassName("errorRegistrazione")[7].style.display = "block";
			document.getElementsByClassName("errorRegistrazione")[6].style.display = "none";
			return;
		} else {
			document.getElementsByClassName("errorRegistrazione")[7].style.display = "none";
		}
		
		// Se il campo username risulta convalidato, eseguo la chiamata AJAX per verificare la disponibilità dell'username inserito. Nel caso non lo sia, mostro un messaggio d'errore
		// Nel caso il campo username non sia convalidato , mostro un messaggio d'errore
		if(document.getElementById("username").checkValidity() === false){
			document.getElementsByClassName("errorRegistrazione")[6].style.display = "block";
			document.getElementsByClassName("errorRegistrazione")[5].style.display = "none";
			return;
		} else {
			document.getElementById("username").classList.add("loading");
			var username = document.getElementById("username").value;
			var xhml = new XMLHttpRequest();
			xhml.onreadystatechange = function(){
				if(xhml.readyState == 4){
					switch(xhml.status){
						case 471:
							document.getElementById("username").classList.remove("loading");
							document.getElementsByClassName("errorRegistrazione")[5].style.display = "block";
							// Verifico se bisogna attivare/disattivare il pulsante di registrazione
							activeSubmitRegistrazione();
							break;				
						case 200:
							document.getElementById("username").classList.remove("loading");
							document.getElementsByClassName("errorRegistrazione")[5].style.display = "none";
							// Verifico se bisogna attivare/disattivare il pulsante di registrazione
							activeSubmitRegistrazione();
							break;
						case 472:
							// mostro l'errore 11, diabilita il pulsante annulla e attendo 8 secondi e richiamo annullaRegistrazione
							document.getElementsByClassName("errorRegistrazione")[11].style.display = "block";
							document.getElementById("AnnullaRegistrazione").disabled = true;
							document.getElementById("AnnullaRegistrazione").style.cursor = "default";
							setTimeout(annullaRegistrazione, 8000);
							break;
						default:
							// mostro l'errore 11, diabilita il pulsante annulla e attendo 8 secondi e richiamo annullaRegistrazione
							document.getElementsByClassName("errorRegistrazione")[12].innerHTML = "Error " + xhml.status + ". A breve verrai reindirizzato alla pagina iniziale";
							document.getElementsByClassName("errorRegistrazione")[12].style.display = "block";
							document.getElementById("AnnullaRegistrazione").disabled = true;
							document.getElementById("AnnullaRegistrazione").style.cursor = "default";
							setTimeout(annullaRegistrazione, 8000);
					}
				}
			};
			xhml.open("GET","CheckUser?username=" + username,Boolean(async));
			xhml.send(); 
		}		
	}
	
	// Funzione per verificare che i campi password e ripetiPassword siano coincidenti
	document.getElementById("ripetiPassword").addEventListener("keyup",checkPassword);
	document.getElementById("password").addEventListener("keyup",checkPassword);
	function checkPassword(){
		// Nel caso in cui i campi siano entrambi vuoti, non mostro nulla
		if(document.getElementById("ripetiPassword").value === "" && document.getElementById("password").value === ""){
			document.getElementsByClassName("errorRegistrazione")[8].style.display = "none";
			document.getElementsByClassName("errorRegistrazione")[9].style.display = "none";
			document.getElementsByClassName("errorRegistrazione")[10].style.display = "none";
			return;
		} 
		// Nel caso in cui ripetiPassword non sia vuoto, ma password sì, mostro messaggio d'errore "Inserisci password"
		else if(document.getElementById("ripetiPassword").value !== "" && document.getElementById("password").value === ""){
			document.getElementsByClassName("errorRegistrazione")[8].style.display = "block";
		} 
		// Nel caso in cui entrambi i campi siano compilati, rimuovo messaggio d'errore "Inserisci password"
		else {
			document.getElementsByClassName("errorRegistrazione")[8].style.display = "none";
		}
		
		// Se il campo password o ripetiPassword non sono convalidati, mostro messaggio d'errore
		// Se entrambi i campi sono convalidati, verifico che siano uguali. Nel caso non lo fossero, mostro messaggio d'errore "Password non coincidenti"
		if(document.getElementById("password").checkValidity() === false){
			document.getElementsByClassName("errorRegistrazione")[9].style.display = "block";
			activeSubmitRegistrazione();
		} else {
			document.getElementsByClassName("errorRegistrazione")[9].style.display = "none";
			var password = document.getElementById("password").value;
			var ripetiPassword = document.getElementById("ripetiPassword").value;
			if(password !== ripetiPassword){
				document.getElementsByClassName("errorRegistrazione")[10].style.display = "block";
				activeSubmitRegistrazione();
			} else {
				document.getElementsByClassName("errorRegistrazione")[10].style.display = "none";
				activeSubmitRegistrazione();
			}
				
		}
	}
	
	// Funzione che verifica le condizioni per l'attivazione del pulsante di registrazione 
	// Condizione 1 - Tutti i campi della form siano sintatticamente validi
	// Condizione 2 - Non vi siano errori mostrati a video
	document.getElementById("nome").addEventListener("keyup",activeSubmitRegistrazione);
	document.getElementById("cognome").addEventListener("keyup",activeSubmitRegistrazione);
	document.getElementById("username").addEventListener("keyup",activeSubmitRegistrazione);
	document.getElementById("mail").addEventListener("keyup",activeSubmitRegistrazione);
	function activeSubmitRegistrazione(){
		// Condizione 1
		if(document.getElementById("nome").checkValidity() === false && document.getElementById("nome").value !== ""){
			document.getElementsByClassName("errorRegistrazione")[3].style.display = "block";
		} else {
			document.getElementsByClassName("errorRegistrazione")[3].style.display = "none";
		}
		if(document.getElementById("cognome").checkValidity() === false && document.getElementById("cognome").value !== ""){
			document.getElementsByClassName("errorRegistrazione")[4].style.display = "block";
		} else {
			document.getElementsByClassName("errorRegistrazione")[4].style.display = "none";
		}
		if(document.getElementById("username").checkValidity() === false && document.getElementById("username").value !== ""){
			document.getElementsByClassName("errorRegistrazione")[7].style.display = "none";
			document.getElementsByClassName("errorRegistrazione")[6].style.display = "block";
		} else {
			document.getElementsByClassName("errorRegistrazione")[6].style.display = "none";
		}
		if(document.getElementById("mail").checkValidity() === false && document.getElementById("mail").value !== ""){
			document.getElementsByClassName("errorRegistrazione")[0].style.display = "none";
			document.getElementsByClassName("errorRegistrazione")[1].style.display = "block";
		} else {
			document.getElementsByClassName("errorRegistrazione")[1].style.display = "none";
		}
		
		// Condizione 2
		if(document.getElementById("RegistrazioneForm").checkValidity() === true){
			var attiva = true;
			for(var i=0; i<document.getElementsByClassName("errorRegistrazione").length; i++){
				if(document.getElementsByClassName("errorRegistrazione")[i].style.display === "block"){
					attiva = false;
					break;
				}
			}
			if(attiva === true){
				document.getElementById("RegistrazioneButton").disabled = false;
				document.getElementById("RegistrazioneButton").style.cursor = "pointer";
			} else {
				document.getElementById("RegistrazioneButton").disabled = true;
				document.getElementById("RegistrazioneButton").style.cursor = "default";
			}
		} else {
			document.getElementById("RegistrazioneButton").disabled = true;
			document.getElementById("RegistrazioneButton").style.cursor = "default";
		}
	}
	
	// Utilizzo del tasto Invio per il pulsante Registrati
	addEventListener('keypress', function(e) {
		if (e.key === 'Enter') {
			if(document.getElementById("ModalRegistrazione").offsetWidth > 0 && document.getElementById("ModalRegistrazione").offsetHeight > 0){
				activeSubmitRegistrazione();
				if(document.getElementById("RegistrazioneButton").disabled === false){
					checkEmail(false);
					checkUser(false);
					registraUtente();
				}
			}
		}
	});
	
	// Funzione che passa al server i dati del nuovo utente da inserire nel database
	// Nel caso la registrazione avvenga con successo viene mostrato un messaggio di conferma all'utente, che deve premere OK
	document.getElementById("RegistrazioneButton").addEventListener("click",registraUtente);
	function registraUtente(){
		// Verifico che non vi siano errori mostrati a video. Nel caso vi siano, disattivo il pulsante di registrazione e interrompo la funzione
		var attiva = true;
		for(var i=0; i<document.getElementsByClassName("errorRegistrazione").length; i++){
			if(document.getElementsByClassName("errorRegistrazione")[i].style.display === "block"){
				attiva = false;
				break;
			}
		}
		if(attiva === false){
			document.getElementById("RegistrazioneButton").disabled = true;
			document.getElementById("RegistrazioneButton").style.cursor = "default";
			return;
		} else {
			var form = document.getElementById("RegistrazioneForm");
			var mail = form[0].value;
			var nome = form[1].value;
			var cognome = form[2].value;
			var username = form[3].value;
			var password = form[4].value;
			var ripetiPassword = form[5].value;
			var xhml = new XMLHttpRequest();
			xhml.onreadystatechange = function(){
				if(xhml.readyState == 4){
					switch(xhml.status){
						case 472:
							// mostro l'errore 11, diabilita il pulsante annulla e attendo 8 secondi e richiamo annullaRegistrazione
							document.getElementsByClassName("errorRegistrazione")[11].style.display = "block";
							document.getElementById("RegistrazioneButton").disabled = true;
							document.getElementById("RegistrazioneButton").style.cursor = "default";
							document.getElementById("AnnullaRegistrazione").disabled = true;
							document.getElementById("AnnullaRegistrazione").style.cursor = "default";
							setTimeout(annullaRegistrazione, 8000);
							break;
						case 200:
							document.getElementsByClassName("errorRegistrazione")[11].style.display = "none";
							document.getElementById("ModalRegistrazione").style.display = "none";
							document.getElementById("RegistrazioneForm").reset();
							document.getElementById("messaggioSuccesso").style.display = "block";
							break;
						default:
							// mostro l'errore 11, diabilita il pulsante annulla e attendo 8 secondi e richiamo annullaRegistrazione
							document.getElementsByClassName("errorRegistrazione")[12].innerHTML = "Error " + xhml.status + ". A breve verrai reindirizzato alla pagina iniziale";
							document.getElementsByClassName("errorRegistrazione")[12].style.display = "block";
							document.getElementById("AnnullaRegistrazione").disabled = true;
							document.getElementById("AnnullaRegistrazione").style.cursor = "default";
							setTimeout(annullaRegistrazione, 8000);
					}
				}
			}
			xhml.open("POST","SignUpUser",true);
			xhml.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
			xhml.send('mail=' + mail + '&nome=' + nome + '&cognome=' + cognome + '&username=' + username + '&password=' + password + '&ripetiPassword=' + ripetiPassword);
		}
	}
	
	// La pressione del pulsante OK chiude la pagina modale della registrazione
	document.getElementById("OkButton").addEventListener("click",function(){
		document.getElementById("messaggioSuccesso").style.display = "none";
		document.getElementById("RegistrazioneButton").disabled = true;
	});
	
	// Funzione che chiude la finestra modale per la registrazione dell'utente, resetta i campi della form, nasconde gli errori,
	// disattiva il pulsante di registrazione e ne imposta lo stile del cursore sul valore di default
	document.getElementById("AnnullaRegistrazione").addEventListener("click",annullaRegistrazione);
	function annullaRegistrazione(){
		document.getElementById("ModalRegistrazione").style.display = "none";
		document.getElementById("RegistrazioneForm").reset();
		document.getElementById("mail").classList.remove("loading");
		document.getElementById("username").classList.remove("loading");		
		for(var i=0; i<document.getElementsByClassName("errorRegistrazione").length; i++){
			document.getElementsByClassName("errorRegistrazione")[i].style.display = "none";
		}
		document.getElementById("RegistrazioneButton").disabled = true;
		document.getElementById("RegistrazioneButton").style.cursor = "default";
		document.getElementById("AnnullaRegistrazione").disabled = false;
		document.getElementById("AnnullaRegistrazione").style.cursor = "pointer";
	}
	
})();