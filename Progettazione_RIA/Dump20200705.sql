CREATE DATABASE  IF NOT EXISTS `heroku_f0de82868ddb446` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `heroku_f0de82868ddb446`;
-- MySQL dump 10.13  Distrib 8.0.19, for Win64 (x86_64)
--
-- Host: eu-cdbr-west-02.cleardb.net    Database: heroku_f0de82868ddb446
-- ------------------------------------------------------
-- Server version	5.6.47-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `invitato`
--

DROP TABLE IF EXISTS `invitato`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `invitato` (
  `idUtente` int(11) NOT NULL,
  `idRiunione` int(11) NOT NULL,
  PRIMARY KEY (`idUtente`,`idRiunione`),
  KEY `idRiunione_idx` (`idRiunione`),
  CONSTRAINT `idRiunione` FOREIGN KEY (`idRiunione`) REFERENCES `riunione` (`idRiunione`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `idUtente` FOREIGN KEY (`idUtente`) REFERENCES `utente` (`idUtente`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invitato`
--

LOCK TABLES `invitato` WRITE;
/*!40000 ALTER TABLE `invitato` DISABLE KEYS */;
INSERT INTO `invitato` VALUES (2,1),(3,1),(4,1),(2,2),(3,2),(2,3),(3,3),(4,3),(1,4),(4,4),(1,5),(4,5),(1,6),(1,7),(1,8),(2,8),(3,8),(1,9),(2,9),(3,9),(1,10),(2,10),(3,10),(2,11),(3,11),(4,11),(1,12),(3,12),(4,12),(1,13),(2,13),(4,13),(1,14),(3,14),(1,15),(3,15),(4,15),(1,16),(3,16),(4,16),(1,19),(4,19),(3,20),(4,20),(1,21),(3,21),(4,21),(3,22),(4,23),(1,24),(2,24),(3,24),(1,27),(2,27),(3,27),(2,70),(3,70),(4,70),(5,70),(6,70),(8,70),(10,70),(12,70),(31,70),(2,71),(3,71),(4,71),(1,72),(3,72),(4,72),(6,72),(8,72),(10,72),(12,72),(31,72),(1,73),(3,73),(4,73),(1,74),(2,74),(4,74),(5,74),(6,74),(7,74),(8,74),(9,74),(10,74),(11,74),(12,74),(21,74),(31,74),(1,75),(2,75),(4,75),(1,76),(2,76),(3,76),(5,76),(8,76),(11,76),(31,76),(1,77),(2,77),(3,77),(7,78),(9,78),(11,78),(21,78),(6,79),(8,79),(10,79),(5,80),(7,80),(8,80),(9,80),(10,80),(11,80),(12,80),(21,80),(31,80),(11,81),(12,81),(21,81);
/*!40000 ALTER TABLE `invitato` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `riunione`
--

DROP TABLE IF EXISTS `riunione`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `riunione` (
  `idRiunione` int(11) NOT NULL,
  `titolo` varchar(45) NOT NULL,
  `dataeora` datetime NOT NULL,
  `durata` time NOT NULL,
  `numPartecipanti` int(11) NOT NULL,
  `organizzatore` int(11) NOT NULL,
  PRIMARY KEY (`idRiunione`),
  KEY `organizzatore_idx` (`organizzatore`),
  CONSTRAINT `organizzatore` FOREIGN KEY (`organizzatore`) REFERENCES `utente` (`idUtente`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `riunione`
--

LOCK TABLES `riunione` WRITE;
/*!40000 ALTER TABLE `riunione` DISABLE KEYS */;
INSERT INTO `riunione` VALUES (1,'Meeting Skype','2020-04-14 10:15:00','02:00:00',3,1),(2,'Scrum','2020-04-16 08:15:00','00:15:00',2,1),(3,'Avanzamento Progetto 1','2020-04-17 14:00:00','03:00:00',3,1),(4,'Specifiche Progetto 2','2020-04-20 08:30:00','04:00:00',2,2),(5,'Avanzamento Progetto 2','2020-05-01 14:00:00','03:00:00',2,2),(6,'Specifiche Progetto 3','2020-04-14 15:00:00','02:00:00',1,3),(7,'Avanzamento Progetto 3','2020-04-24 09:00:00','03:00:00',1,3),(8,'Specifiche Progetto 4','2020-05-04 10:00:00','02:00:00',3,4),(9,'Avanzamento Progetto 4','2020-05-15 14:00:00','04:00:00',3,4),(10,'Discussione Budget','2020-04-15 10:00:00','02:30:00',3,4),(11,'Rendiconto Quadrimestrale','2020-04-30 08:30:00','04:00:00',3,1),(12,'Videocall ','2020-04-23 10:45:00','01:00:00',3,2),(13,'Testing Implementazione','2020-04-28 15:15:00','00:45:00',3,3),(14,'Analisi Dei Dati','2020-04-22 15:30:00','01:00:00',2,2),(15,'Analisi Dei Requisiti','2020-04-20 11:00:00','02:00:00',3,2),(16,'Revisione Progetto','2020-04-14 10:15:00','03:00:00',3,2),(19,'Revisione Finale','2020-04-22 10:30:00','01:00:00',2,2),(20,'Videocall','2020-06-10 15:07:00','01:30:00',2,1),(21,'Discussione Progetto','2020-05-14 10:00:00','02:00:00',3,2),(22,'Revisione IFML','2020-04-19 15:00:00','01:00:00',1,2),(23,'Revisione Diagrammi','2020-04-20 12:00:00','01:00:00',1,2),(24,'Discussione finale','2020-06-15 10:00:00','03:30:00',3,4),(27,'Discussione nuovo progetto','2020-06-23 10:15:00','02:00:00',3,4),(70,'Meeting Skype','2020-07-07 09:30:00','01:30:00',9,1),(71,'Discussione progetto','2020-07-10 14:00:00','00:30:00',3,1),(72,'Analisi bilancio','2020-07-14 11:15:00','03:00:00',8,2),(73,'Riunione dipartimento','2020-07-10 10:00:00','02:00:00',3,2),(74,'Discussione trimestre','2020-09-30 09:00:00','04:00:00',13,3),(75,'Daily scrum','2020-07-13 08:15:00','00:15:00',3,3),(76,'Stanziamento fondi','2020-07-08 15:30:00','02:00:00',7,4),(77,'Progettazione','2020-07-16 10:30:00','01:00:00',3,4),(78,'Stato progetto','2020-07-22 11:30:00','01:30:00',4,5),(79,'Meeting Teams','2020-07-20 09:30:00','02:00:00',3,5),(80,'Apertura divisione','2020-08-03 12:00:00','01:00:00',9,6),(81,'Stato lavori','2020-07-28 10:00:00','01:00:00',3,7);
/*!40000 ALTER TABLE `riunione` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `utente`
--

DROP TABLE IF EXISTS `utente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `utente` (
  `idUtente` int(11) NOT NULL AUTO_INCREMENT,
  `mail` varchar(45) NOT NULL,
  `username` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `nome` varchar(45) NOT NULL,
  `cognome` varchar(45) NOT NULL,
  PRIMARY KEY (`idUtente`),
  UNIQUE KEY `unique_index` (`mail`,`username`),
  UNIQUE KEY `mail_UNIQUE` (`mail`),
  UNIQUE KEY `username_UNIQUE` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `utente`
--

LOCK TABLES `utente` WRITE;
/*!40000 ALTER TABLE `utente` DISABLE KEYS */;
INSERT INTO `utente` VALUES (1,'mail1@dominio.it','luciano.bozzini','Password1','Luciano','Bozzini'),(2,'mail2@dominio.it','danilo.falconieri','Password2','Danilo','Falconieri'),(3,'mail3@dominio.it','gioele.demarchi','Password3','Gioele','Demarchi'),(4,'mail4@dominio.it','tommaso.terenghi','Password4','Tommaso','Terenghi'),(5,'mario.rossi@hotmail.it','mario.rossi','Password5','Mario','Rossi'),(6,'luca.bianchi@gmail.com','luca.bianchi','Password6','Luca','Bianchi'),(7,'salvatore.esposito@yahoo.com','salvatore.esposito','Password7','Salvatore','Esposito'),(8,'andrea.molteni@live.com','andrea.molteni','Password8','Andrea','Molteni'),(9,'marco.fumagalli@polimi.it','marco.fumagalli','Password9','Marco','Fumagalli'),(10,'paolo.brambilla@libero.it','paolo.brambilla','Password10','Paolo','Brambilla'),(11,'antonio.mascetti@alice.it','antonio.mascetti','Password11','Antonio','Mascetti'),(12,'andrea.roncoroni@gmail.com','andrea.roncoroni','Password12','Andrea','Roncoroni'),(21,'michele_carughi@hotmail.it','michele.carughi','Password13','Michele','Carughi'),(31,'paola.casati@live.com','paola.casati','Password14','Paola','Casati');
/*!40000 ALTER TABLE `utente` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-07-05 12:36:32
